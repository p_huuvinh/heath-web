const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    creator: {
        id: String,
        name: {
            First: String,
            Last: String
        }
    },
    message: String,
    time: String,
    comment: Array
});

module.exports = mongoose.model('posts', postSchema);