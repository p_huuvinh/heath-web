const mongoose = require('mongoose');
var bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
  login: {
    email: String,
    password: String,
  },
  name: {
  	First: String,
  	Last: String,
  },
  detail: {
    avatar: String,
    date_of_birth: Date,
    age: Number,
    phone: String,
    account_type: String,
    address: String
  },
  comment: Array,
  index: Array
});

userSchema.statics.get_blood_pressure = function(id, date, data_type) {
  var user = this;


}

userSchema.statics.authenticate = function (email, password, callback) {
  var User = this;
  User.findOne({ 'login.email': email })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.login.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

//hashing a password before saving it to the database
userSchema.pre('save', function (next) {
  var user = this;
  console.log("pre");
  bcrypt.hash(user.login.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.login.password = hash;
    next();
  })
});

module.exports = mongoose.model('users', userSchema);
