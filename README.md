# Node.js-Register-Login-App

This is a user login and registration app using Node.js, Express, Mongoose and express-sessions. :+1:

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. :heart:

### Prerequisites

You need to have installed Node.js, NPM and MongoDB in your System.

### Installing
```
npm install
```

### To Run
```
nodemon index.js
```
### If error, then run command below to check and fix
````
 npm audit
````
 
Code is Running on 
+ http://localhost:8888/

code is public on
+ http://3.0.142.116:8888/
+ 
:heart_eyes: Thanks

