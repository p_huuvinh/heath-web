var express = require('express');
var router = express.Router();

const User = require('../models/users');
const userPost = require('../models/post');

/* GET home page. */
router.get('/', function (req, res, next) {

	if (!req.session.userId) {
		res.render('index', {
			title: 'Health - Homepage',
			account: 'Sign in',
			condition: false
		});
	}
	else {
		User.findById(req.session.userId, function (err, users) {
			userPost.find({},null,{
				skip:0, // Starting Row
   				limit:5, // Ending Row
				sort:{
					time: -1 //Sort by Date Added DESC
				}
			},function(err,post){
				console.log(users);	
				res.render('homepage', {
					title: 'Health - Homepage',
					account: users,
					index: users.index,
					post: post,
					condition: false
				});				
		});
	});
}
	console.log('user accessing Home page');
});

router.get('/home', function (req, res, next) {
	res.render('homepage', {
		title: 'Health - home',
		account: 'Sign in',
		condition: false
	});
});

router.get('/charts', function (req, res, next) {
	res.render('charts', { title: 'Health - chart', condition: false });
	console.log('user accessing About us page');
});

router.get('/news-feed', function (req, res, next) {
	if (!req.session.userId) {
		res.render('index', {
			title: 'Health - Homepage',
			condition: false
		});
	}
	else {
		User.findById(req.session.userId, function (err, users) {
			userPost.find({},null,{
				skip:0, // Starting Row
   				limit:5, // Ending Row
				sort:{
					time: -1 //Sort by Date Added DESC
				}
			},function(err,post){
				console.log(users);	
				console.log(post);
				res.render('news-feed', {
					title: 'Health - Homepage',
					account: users,
					post: post,
					condition: false
				});				
			});
		});
	}
	console.log('user accessing Home page');
});

router.get('/profile', function (req, res, next) {
	User.findById(req.session.userId, function (err, users) {
		res.render('profile', {
			title: 'Health - Profile',
			account: users,
			condition: false
		});
		console.log(users);
	});
	console.log('user accessing profile page');
});

router.get('/tables', function (req, res, next) {
	User.findById(req.session.userId, function (err, users) {
		res.render('tables', {
			title: 'Health - Homepage',
			account: users,
			index: users.index,
			condition: false
		});
		console.log(users);
	});
	console.log('user accessing tables page');
});

router.get('/sign-up', function (req, res, next) {
	res.render('sign-up', { title: 'Health - Sign Up', condition: false });
	console.log('user accessing sign up page');
});

router.get('/login', function (req, res, next) {
	if (!req.session.userId) {
		res.render('login', { title: 'Health - log In', condition: false });
		console.log('user accessing login page');
	}
	else {
		res.redirect('/');
	}
});

router.get('/logout', function (req, res, next) {
	if (req.session.userId) {
		// delete session object
		req.session.destroy(function (err) {
			if (err) {
				return next(err);
			}
		});
		return res.redirect('/');
	}
});

router.get('/register', function (req, res, next) {
	res.render('register', { title: 'Health - Register', condition: false });
	console.log('user accessing register page');
});

router.get('/forgot-password', function (req, res, next) {
	res.render('forgot-password', { title: 'Health - Forgot password', condition: false });
	console.log('user accessing forgot page');
});

router.get('/data', function (req, res, next) {
	var email = req.param('email');
	var password = req.param('password');
	var blood = req.param('blood');
	var oxygen = req.param('oxygen');
	var pulse = req.param('pulse');
	var temp = req.param('temp');
	console.log(password);
	User.authenticate(email, password, function (error, user) {
		if (error || !email) {
			console.log("auth failed");
			var err = new Error('Wrong email or password.');
			err.status = 401;
			return next(err);
		} else {
			user.update({
				$push: {
					'index':
						{
							'Pulse': pulse,
							'Blood_pressure': blood,
							'Oxygen': oxygen,
							'Temp' : temp,
							'Date': (new Date(Date.now())).toLocaleString('en-US')
						}
				}
			}
			).exec();
			console.log("Save success!!");
			return res.send(user);;
		}
	});

});


router.get('/show', function(req, res, next) {
	userPost.find({},null,{
		skip:0, // Starting Row
		limit:1, // Ending Row
		sort:{
			time: 1 //Sort DESC
		}
	},function(err, response){
		console.log('abc');
	  	console.log(response);
	  	res.json(response);
	});
  });


router.post('/register', function (req, res, next) {
	console.log('user submit register ');
	console.log(req.body);
	var personInfo = req.body;
	if (!personInfo.InputName || !personInfo.InputLastName || !personInfo.InputEmail || !personInfo.InputPassword || !personInfo.ConfirmPassword) {
		res.send();
	}
	else {
		if (personInfo.InputPassword == personInfo.ConfirmPassword) {
			User.findOne({ 'login.email': personInfo.InputEmail }, function (err, data) {
				if (!data) {
					User.findOne({}, function (err, data) {
						var newUser = new User({
							login: {
								email: personInfo.InputEmail,
								password: personInfo.InputPassword
							},
							name: {
								First: personInfo.InputName,
								Last: personInfo.InputLastName
							},
							detail: {
								date_of_birth: personInfo.InputDateOfBirth,
								phone: personInfo.InputPhone,
								accout_type: 'normal'
							}
						});
						newUser.save(function (err, Person) {
							if (err)
								console.log(err);
							else
								console.log('Save! Success');
						});
					}).sort({ _id: -1 }).limit(1);
					res.redirect("/login");
				}
				else {
					res.send({ "Success": "Email is already used." });
				}
			});
		}
	}
});

router.post('/update_profile', function (req,res,next){
	console.log(req.body);
	var updateInfo = req.body;

});

router.post('/login', function (req, res, next) {
	console.log(req.body);
	var loginInfo = req.body;
	User.authenticate(loginInfo.InputEmail, loginInfo.InputPassword, function (error, user) {
		if (error || !user) {
			var err = new Error('Wrong email or password.');
			err.status = 401;
			return next(err);
		} else {
			req.session.userId = user._id;
			if(loginInfo.device == "phone")
			{
				return res.send(user);
			}
			return res.redirect('/');
		}
	});
});

router.post('/status', function (req, res, next) {
	console.log(req.body);
	console.log(req.session.userId);
	var postOnWeb = req.body;
	var newUserPost;
	User.findById(req.session.userId, function (err, users) {
		console.log(users.name);
		newUserPost= new userPost({
			creator: {
				id: req.session.userId,
				name: users.name
			},
			message: postOnWeb.InputPost,
			time: (new Date(Date.now())).toLocaleString('en-US')	
		});
		newUserPost.save(function (err, post) {
			if (err)
				console.log(err);
			else
				console.log('Status saved');
		});		
	});
	res.redirect('/news-feed');
	// res.json({Success:'1'});
});

router.post('/data', function (req, res, next) {
	console.log("Post from phone");
	var personInfo = req.body;
	var pulse = personInfo.Pulse;
	var blood = personInfo.Blood_pressure;
	var oxygen = personInfo.Oxygen;
	var temp = personInfo.Temp;
	var email = personInfo.email;
	var password = personInfo.password;
	console.log(personInfo);

	User.authenticate(email, password, function (error, user) {
		if (error || !email) {
			var err = new Error('Wrong email or password.');
			err.status = 401;
			return next(err);
		} else {
			console.log("success");
			user.update({
				$push: {
					index:
						{
							Pulse: pulse,
							Blood_pressure: blood,
							Oxygen: oxygen,
							Temp : temp,
							Date: (new Date(Date.now())).toLocaleString('en-US')
						}
				}
			}
			).exec();
			return res.send('Success!!');
		}
	});
	// res.json({Success:'1'});
});
router.put('/user/:id', function(req, res) {
  var id = req.params.id;
  console.log("id"+id);
  var personInfo = req.body;
  console.log()

  User.update({unique_id:id}, {
    username: personInfo.username, 
    fullname: personInfo.fullname, 
    age: personInfo.age
  }, function(err, rawResponse) {
   console.log(rawResponse);
 });

});

module.exports = router;
